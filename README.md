# CountryViewer by Rafał Drezner

Used technologies, frameworks and libs:
* Frameworks: Angular 5, ngrx (store, effects, router-store), RxJS.
* UI: Angular Material.
* Tests: Karma/Jasmine.
* CI: Gitlab-CI ([pipelines](https://gitlab.com/rdrezner/country-viewer/pipelines))
* Additional libs: ngx-translate (i18n), ngx-loading (preloader), rest-url-builder.

Live demo ([view](http://drezner.com.pl/ciklum)).

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/)
* [Yarn](https://yarnpkg.com/lang/en/) 
* [Angular CLI](https://github.com/angular/angular-cli)

## Installation

* `git clone <repository-url>` this repository
* `cd country-viewer`
* `yarn`

## Running / Development

* `yarn start`
* Visit your app at [http://localhost:4200](http://localhost:4200).


## Running Tests

* `yarn test`
