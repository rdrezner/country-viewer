import {CountryDetailsVo} from '../app/model/country-details-vo';
import {CountryBriefDataVo} from '../app/model/country-brief-data-vo';

export const countriesListMock: CountryBriefDataVo[] = <CountryBriefDataVo[]>JSON.parse(`
  [
    {
      "name": "Afghanistan",
      "topLevelDomain": [
        ".af"
      ],
      "alpha2Code": "AF",
      "alpha3Code": "AFG",
      "callingCodes": [
        "93"
      ],
      "capital": "Kabul",
      "altSpellings": [
        "AF",
        "Afġānistān"
      ],
      "region": "Asia",
      "subregion": "Southern Asia",
      "population": 26023100,
      "latlng": [
        33,
        65
      ],
      "demonym": "Afghan",
      "area": 652230,
      "gini": 27.8,
      "timezones": [
        "UTC+04:30"
      ],
      "borders": [
        "IRN",
        "PAK",
        "TKM",
        "UZB",
        "TJK",
        "CHN"
      ],
      "nativeName": "افغانستان",
      "numericCode": "004",
      "currencies": [
        "AFN"
      ],
      "languages": [
        "ps",
        "uz",
        "tk"
      ],
      "translations": {
        "de": "Afghanistan",
        "es": "Afganistán",
        "fr": "Afghanistan",
        "ja": "アフガニスタン",
        "it": "Afghanistan"
      },
      "relevance": "0"
    },
    {
      "name": "Åland Islands",
      "topLevelDomain": [
        ".ax"
      ],
      "alpha2Code": "AX",
      "alpha3Code": "ALA",
      "callingCodes": [
        "358"
      ],
      "capital": "Mariehamn",
      "altSpellings": [
        "AX",
        "Aaland",
        "Aland",
        "Ahvenanmaa"
      ],
      "region": "Europe",
      "subregion": "Northern Europe",
      "population": 28875,
      "latlng": [
        60.116667,
        19.9
      ],
      "demonym": "Ålandish",
      "area": 1580,
      "gini": null,
      "timezones": [
        "UTC+02:00"
      ],
      "borders": [],
      "nativeName": "Åland",
      "numericCode": "248",
      "currencies": [
        "EUR"
      ],
      "languages": [
        "sv"
      ],
      "translations": {
        "de": "Åland",
        "es": "Alandia",
        "fr": "Åland",
        "ja": "オーランド諸島",
        "it": "Isole Aland"
      },
      "relevance": "0"
    },
    {
      "name": "Albania",
      "topLevelDomain": [
        ".al"
      ],
      "alpha2Code": "AL",
      "alpha3Code": "ALB",
      "callingCodes": [
        "355"
      ],
      "capital": "Tirana",
      "altSpellings": [
        "AL",
        "Shqipëri",
        "Shqipëria",
        "Shqipnia"
      ],
      "region": "Europe",
      "subregion": "Southern Europe",
      "population": 2893005,
      "latlng": [
        41,
        20
      ],
      "demonym": "Albanian",
      "area": 28748,
      "gini": 34.5,
      "timezones": [
        "UTC+01:00"
      ],
      "borders": [
        "MNE",
        "GRC",
        "MKD",
        "KOS"
      ],
      "nativeName": "Shqipëria",
      "numericCode": "008",
      "currencies": [
        "ALL"
      ],
      "languages": [
        "sq"
      ],
      "translations": {
        "de": "Albanien",
        "es": "Albania",
        "fr": "Albanie",
        "ja": "アルバニア",
        "it": "Albania"
      },
      "relevance": "0"
    },
    {
      "name": "Algeria",
      "topLevelDomain": [
        ".dz"
      ],
      "alpha2Code": "DZ",
      "alpha3Code": "DZA",
      "callingCodes": [
        "213"
      ],
      "capital": "Algiers",
      "altSpellings": [
        "DZ",
        "Dzayer",
        "Algérie"
      ],
      "region": "Africa",
      "subregion": "Northern Africa",
      "population": 39500000,
      "latlng": [
        28,
        3
      ],
      "demonym": "Algerian",
      "area": 2381741,
      "gini": 35.3,
      "timezones": [
        "UTC+01:00"
      ],
      "borders": [
        "TUN",
        "LBY",
        "NER",
        "ESH",
        "MRT",
        "MLI",
        "MAR"
      ],
      "nativeName": "الجزائر",
      "numericCode": "012",
      "currencies": [
        "DZD"
      ],
      "languages": [
        "ar"
      ],
      "translations": {
        "de": "Algerien",
        "es": "Argelia",
        "fr": "Algérie",
        "ja": "アルジェリア",
        "it": "Algeria"
      },
      "relevance": "0"
    }
  ]
    `);

export const countriesDetailsMock: CountryDetailsVo = <CountryDetailsVo>JSON.parse(`
  {
    "name": "Afghanistan",
    "topLevelDomain": [
      ".af"
    ],
    "alpha2Code": "AF",
    "alpha3Code": "AFG",
    "callingCodes": [
      "93"
    ],
    "capital": "Kabul",
    "altSpellings": [
      "AF",
      "Afġānistān"
    ],
    "region": "Asia",
    "subregion": "Southern Asia",
    "population": 26023100,
    "latlng": [
      33,
      65
    ],
    "demonym": "Afghan",
    "area": 652230,
    "gini": 27.8,
    "timezones": [
      "UTC+04:30"
    ],
    "borders": [
      "IRN",
      "PAK",
      "TKM",
      "UZB",
      "TJK",
      "CHN"
    ],
    "nativeName": "افغانستان",
    "numericCode": "004",
    "currencies": [
      "AFN"
    ],
    "languages": [
      "ps",
      "uz",
      "tk"
    ],
    "translations": {
      "de": "Afghanistan",
      "es": "Afganistán",
      "fr": "Afghanistan",
      "ja": "アフガニスタン",
      "it": "Afghanistan"
    },
    "relevance": "0"
  }
`);
