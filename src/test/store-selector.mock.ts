import {StoreSelector} from '../app/store/selector/store-selector';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {CountriesState} from '../app/store/state/countries-state';
import {RouterStateUrl} from '../app/store/serializer/custom-router-state-serializer';
import {RouterReducerState} from '@ngrx/router-store';
import {countriesDetailsMock, countriesListMock} from './countries-api-responses-mock';

export class StoreSelectorMock extends StoreSelector {

  public get countriesState(): Observable<CountriesState> {
    return Observable.of({
      countriesList: countriesListMock,
      currentCountryDetails: countriesDetailsMock,
      conversionRate: 0.034374
    });
  }

  public get routerState(): Observable<RouterReducerState<RouterStateUrl>> {
    return Observable.of({
      state: {
        url: '',
        params: {},
        queryParams: {}
      },
      navigationId: 0
    });
  }
}
