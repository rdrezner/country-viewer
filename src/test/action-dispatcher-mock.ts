import {ActionDispatcher} from '../app/store/dispatcher/action-dispatcher';

export class ActionDispatcherMock extends ActionDispatcher {
  dispatch(action: { type: string }): void {
  }
}
