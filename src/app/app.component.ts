import {Component, Inject, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {APP_CONFIG, AppConfig} from './config/app-config.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(translate: TranslateService,
              @Inject(APP_CONFIG) private config: AppConfig) {
    translate.setDefaultLang(this.config.defaultLocale);
    translate.use(this.config.defaultLocale);
  }

  ngOnInit(): void {
  }
}
