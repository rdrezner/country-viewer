/**
 * Route names constants
 */
export class RouteNames {
  public static readonly COUNTRIES = 'countries';
  public static readonly COUNTRY_DETAILS = 'country-details';
  public static readonly COUNTRY_DETAILS_PARAM_CODE = 'code';
  public static readonly ERROR = 'error';
  public static readonly ERROR_PARAM_CODE = 'errorCode';

}
