import {InjectionToken, NgModule} from '@angular/core';

export let APP_CONFIG = new InjectionToken<AppConfig>('APP_CONFIG');

export class AppConfig {
  defaultLocale: string;
  allCountriesEndpoint: string;
  countryDetailsEndpoint: string;
  currencyConverterEndpoint: string;
  defaultToCurrency: string;
  restHeaders: { [index: string]: string };
  paginatorConfig: { pageSize: number, pageSizeOptions: number[] };
  i18nPath: string;
}

export const APP_DI_CONFIG: AppConfig = {
  defaultLocale: 'en',
  allCountriesEndpoint: 'https://restcountries-v1.p.mashape.com/all',
  countryDetailsEndpoint: 'https://restcountries-v1.p.mashape.com/alpha/:code',
  currencyConverterEndpoint: 'https://bravenewcoin-v1.p.mashape.com/convert?from=:from&qty=:qty&to=:to',
  defaultToCurrency: 'PLN',
  restHeaders: {
    'X-Mashape-Key': 'hQGSwXcywrmshZUgMVsrMajkVLcAp1cAzWRjsnRxsYSt3EJ1Kf',
    'Accept': 'application/json'
  },
  paginatorConfig: {
    pageSize: 10,
    pageSizeOptions: [5, 10, 20]
  },
  i18nPath: './assets/i18n/'
};

@NgModule({
  providers: [
    {provide: APP_CONFIG, useValue: APP_DI_CONFIG}
  ]
})
/**
 * Defines, sets and exports provider for application configuration
 */
export class AppConfigModule {
}
