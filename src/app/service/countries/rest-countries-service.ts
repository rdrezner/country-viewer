import {CountriesService} from './coutries-service';
import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {CountryBriefDataVo} from '../../model/country-brief-data-vo';
import 'rxjs/add/observable/throw';
import {CountryDetailsVo} from '../../model/country-details-vo';
import {APP_CONFIG, AppConfig} from '../../config/app-config.module';
import {RestURLBuilder} from 'rest-url-builder';

@Injectable()
/**
 * Fetches countries' data via RESTful web services
 */
export class RestCountriesService extends CountriesService {

  constructor(private httpClient: HttpClient,
              @Inject(APP_CONFIG) private config: AppConfig) {
    super();
  }

  /**
   * Fetches all countries
   * @returns {Observable<CountryBriefDataVo[]>}
   */
  getAllCountries(): Observable<CountryBriefDataVo[]> {
    return this.httpClient.get<CountryBriefDataVo[]>(this.config.allCountriesEndpoint);
  }

  /**
   * Fetches country details by given country code
   * @param {string} countryCode
   * @returns {Observable<CountryDetailsVo>}
   */
  getCountryDetails(countryCode: string): Observable<CountryDetailsVo> {
    return this.httpClient.get<CountryDetailsVo>(this.buildCountryDetailsUrl(countryCode));
  }

  /**
   * Fetches given currency conversion rate to PLN
   * @param {string} currency
   * @returns {Observable<number>}
   */
  getConversionRate(currency: string): Observable<number> {
    return this.httpClient.get<{ to_quantity: number }>(this.buildCurrencyConverterUrl(currency))
      .map((response: { to_quantity: number }) => response.to_quantity)
      .catch(err => {
        console.log(err);
        return Observable.throw(err);
      });
  }

  /**
   * Builds url for service using template from application config and given code
   * @param {string} code
   * @returns {string}
   */
  private buildCountryDetailsUrl(code: string) {
    const urlBuilder = new RestURLBuilder();
    const builder = urlBuilder.buildRestURL(this.config.countryDetailsEndpoint);
    builder.setNamedParameter('code', code);
    return builder.get();
  }

  /**
   * Builds url for service using template from application config and given currency
   * @param {string} currency
   * @returns {string}
   */
  private buildCurrencyConverterUrl(currency: string) {
    const urlBuilder = new RestURLBuilder();
    const builder = urlBuilder.buildRestURL(this.config.currencyConverterEndpoint);
    builder.setQueryParameter('from', currency.toLowerCase());
    builder.setQueryParameter('qty', '1');
    builder.setQueryParameter('to', this.config.defaultToCurrency.toLowerCase());
    return builder.get();
  }

}
