import {Observable} from 'rxjs/Observable';
import {CountryBriefDataVo} from '../../model/country-brief-data-vo';
import {CountryDetailsVo} from '../../model/country-details-vo';

export abstract class CountriesService {

  getAllCountries(): Observable<CountryBriefDataVo[]> {
    throw new Error('implement abstract class');
  }

  getCountryDetails(countryCode: string): Observable<CountryDetailsVo> {
    throw new Error('implement abstract class');
  }

  getConversionRate(currency: string): Observable<number> {
    throw new Error('implement abstract class');
  }
}
