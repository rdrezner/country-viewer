import {Inject, Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {APP_CONFIG, AppConfig} from '../config/app-config.module';
import {Observable} from 'rxjs/Observable';

@Injectable()
/**
 * Adds headers taken from application config to every Http request
 */
export class RestInterceptor implements HttpInterceptor {

  constructor(@Inject(APP_CONFIG) private config: AppConfig) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: this.config.restHeaders
    });
    return next.handle(request);
  }
}
