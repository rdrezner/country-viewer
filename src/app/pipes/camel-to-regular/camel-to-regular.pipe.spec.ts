import {CamelToRegularPipe} from './camel-to-regular.pipe';

describe('CamelToRegularPipe', () => {

  const fixture = [
    ['CamelCase', 'Camel case'],
    ['camelCase', 'Camel case'],
    ['camelCaseCamel', 'Camel case camel'],
    ['camelCasecamel', 'Camel casecamel'],
    ['2camelCase', '2camel case'],
    ['cCamelCase', 'C camel case'],
    ['CCamelCase', 'C camel case']
  ];

  it('create an instance', () => {
    const pipe = new CamelToRegularPipe();
    expect(pipe).toBeTruthy();
  });

  it('should correctly transform camel to regular case', () => {
    const pipe = new CamelToRegularPipe();
    fixture.forEach(value => {
      expect(pipe.transform(value[0])).toEqual(value[1]);
    });
  });
});
