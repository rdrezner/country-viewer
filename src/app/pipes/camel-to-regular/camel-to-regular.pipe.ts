import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'camelToRegular'
})
/**
 * Transforms camel to regular case (e.g. camelCase -> Camel case)
 */
export class CamelToRegularPipe implements PipeTransform {

  transform(value: string): any {
    value = this.lowerCaseFirstLetter(value);
    return this.capitalizeFirstLetter(value
      .replace(/([A-Z])/g, ' $1')
      .toLowerCase());
  }

  private capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  private lowerCaseFirstLetter(string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
  }

}
