import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'formBeautifier'
})
/**
 * Transforms (beautifies) form outputs
 * (e.g. array(1,2,3)->'1, 2, 3', translationObject(de: 'Polen', en: 'Poland')->'de: Polen, en: Poland')
 */
export class FormBeautifierPipe implements PipeTransform {

  transform(value: any, fieldType: string): any {

    if (fieldType === 'translations' && typeof value === 'object') {
      value = Object.keys(value).map((key: string) => `${key}: ${value[key]}`);
    }

    if (value instanceof Array) {
      return value.join(', ');
    }
    return value;
  }

}
