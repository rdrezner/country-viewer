import {FormBeautifierPipe} from './form-beautifier.pipe';

describe('FormBeautifierPipe', () => {

  const fixture: FBPipeFixture[] = [
    {
      input: {
        value: [
          'AF',
          'Afġānistān'
        ],
        fieldName: 'altSpellings'
      },
      expected: 'AF, Afġānistān'
    },
    {
      input: {
        value: [
          33,
          65
        ],
        fieldName: 'latlng'
      },
      expected: '33, 65'
    },
    {
      input: {
        value: 'Afghan',
        fieldName: 'demonym'
      },
      expected: 'Afghan'
    },
    {
      input: {
        value: 652230,
        fieldName: 'area'
      },
      expected: 652230
    },
    {
      input: {
        value: {
          de: 'Afghanistan',
          es: 'Afganistán',
          fr: 'Afghanistan',
          ja: 'アフガニスタン',
          it: 'Afghanistan'
        },
        fieldName: 'translations'
      },
      expected: 'de: Afghanistan, es: Afganistán, fr: Afghanistan, ja: アフガニスタン, it: Afghanistan'
    }
  ];

  it('create an instance', () => {
    const pipe = new FormBeautifierPipe();
    expect(pipe).toBeTruthy();
  });

  it('should correctly beautify form output', () => {
    const pipe = new FormBeautifierPipe();
    fixture.forEach(value => {
      expect(pipe.transform(value.input.value, value.input.fieldName)).toEqual(value.expected);
    });
  });
});

interface FBPipeFixture {
  input: FBPipeInput;
  expected: any;
}

interface FBPipeInput {
  value: any;
  fieldName: string;
}
