import {CountriesState, INITIAL_COUNTRIES_STATE} from '../state/countries-state';
import {Action} from '@ngrx/store';
import * as CountryActions from '../action/countries-actions';

/**
 * Manages state that is connected with countries
 * @param {CountriesState} currentState
 * @param {Action} action
 * @returns {CountriesState}
 */
export function countriesReducer(currentState: CountriesState = INITIAL_COUNTRIES_STATE, action: Action) {
  console.log(`countriesReducer: ${action.type}`);

  let updatedState: CountriesState;

  switch (action.type) {
    case CountryActions.GET_ALL_COUNTRIES:
      updatedState = Object.assign({}, currentState);
      updatedState.countriesList = [];
      return updatedState;
    case CountryActions.GET_ALL_COUNTRIES_SUCCESS:
      const countries = (<CountryActions.GetAllCountriesSuccess>action).countries;
      updatedState = Object.assign({}, currentState);
      updatedState.countriesList = countries;
      return updatedState;
    case CountryActions.GET_COUNTRY_DETAILS:
      updatedState = Object.assign({}, currentState);
      updatedState.currentCountryDetails = undefined;
      return updatedState;
    case CountryActions.GET_COUNTRY_DETAILS_SUCCESS:
      const countryDetails = (<CountryActions.GetCountryDetailsSuccess>action).countryDetails;
      updatedState = Object.assign({}, currentState);
      updatedState.currentCountryDetails = countryDetails;
      return updatedState;
    case CountryActions.GET_CONVERSION_RATE:
      updatedState = Object.assign({}, currentState);
      updatedState.conversionRate = 0;
      return updatedState;
    case CountryActions.GET_CONVERSION_RATE_SUCCESS:
      const conversionRate = (<CountryActions.GetConversionRateSuccess>action).conversionRate;
      updatedState = Object.assign({}, currentState);
      updatedState.conversionRate = conversionRate;
      return updatedState;
    case CountryActions.GET_COUNTRY_DETAILS_AND_CONVERSION_RATE:
      updatedState = Object.assign({}, currentState);
      updatedState.currentCountryDetails = undefined;
      updatedState.conversionRate = 0;
      return updatedState;
  }

  return currentState;
}
