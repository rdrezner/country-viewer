import {Injectable} from '@angular/core';
import {StoreSelector} from './store-selector';
import {Store} from '@ngrx/store';
import {ApplicationStore} from '../application-store';

@Injectable()
/**
 * Abstraction for selecting data from store
 */
export class NgrxStoreSelector extends StoreSelector {

  constructor(private store: Store<ApplicationStore>) {
    super();
  }

  public get countriesState() {
    return this.store.select(store => store.countriesState);
  }

  public get routerState() {
    return this.store.select(store => store.routerState);
  }
}


