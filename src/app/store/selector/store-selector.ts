import {Observable} from 'rxjs/Observable';
import {CountriesState} from '../state/countries-state';
import {RouterStateUrl} from '../serializer/custom-router-state-serializer';
import {RouterReducerState} from '@ngrx/router-store';

export abstract class StoreSelector {
  countriesState: Observable<CountriesState>;
  routerState: Observable<RouterReducerState<RouterStateUrl>>;
}
