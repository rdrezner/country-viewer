import {RouterStateSerializer} from '@ngrx/router-store';
import {Params, RouterStateSnapshot} from '@angular/router';

/**
 * Router serializer (easy access to router data)
 */
export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    let route = routerState.root;
    while (route.firstChild) {
      route = route.firstChild;
    }

    const {url} = routerState;
    const queryParams = routerState.root.queryParams;
    const params = route.params;

    return {url, params, queryParams};
  }
}

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
}
