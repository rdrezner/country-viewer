import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as ComponentActions from '../action/component-actions';
import * as RouterActions from '../action/router-actions';
import * as CountriesActions from '../action/countries-actions';
import {RouteNames} from '../../config/route-names';

@Injectable()
/**
 * Handles all actions from components
 */
export class ComponentEffects {

  constructor(private actions$: Actions) {
  }

  @Effect()
  countryDetailsBackSelected$ = this.actions$.ofType(ComponentActions.COUNTRY_DETAILS_BACK_SELECTED)
    .map(() => new RouterActions.Go({
      path: [RouteNames.COUNTRIES]
    }));

  @Effect()
  countriesRefreshSelected$ = this.actions$.ofType(ComponentActions.COUNTRIES_REFRESH_SELECTED)
    .map(() => new CountriesActions.GetAllCountries());

  @Effect()
  countriesCountrySelected$ = this.actions$.ofType(ComponentActions.COUNTRIES_COUNTRY_SELECTED)
    .map((action: ComponentActions.CountriesCountrySelected) => new RouterActions.Go({
      path: [RouteNames.COUNTRY_DETAILS, action.countryCode.toLowerCase()]
    }));
}
