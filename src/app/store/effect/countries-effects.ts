import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import * as CountryActions from '../action/countries-actions';
import {GetConversionRate, GetCountryDetails, GetCountryDetailsAndConversionRate} from '../action/countries-actions';
import {CountriesService} from '../../service/countries/coutries-service';
import {CountryDetailsVo} from '../../model/country-details-vo';
import {ActionDispatcher} from '../dispatcher/action-dispatcher';
import {ErrorVo} from '../../model/error-vo';
import * as ErrorActions from '../action/error-actions';
import {Observable} from 'rxjs/Observable';
import {ErrorCode} from '../../model/error-code';

@Injectable()
/**
 * Handles all countries' data fetching actions
 */
export class CountriesEffects {

  constructor(private actions$: Actions,
              private countriesService: CountriesService,
              private actionDispatcher: ActionDispatcher) {
  }

  @Effect()
  getAllCountries$ = this.actions$.ofType(CountryActions.GET_ALL_COUNTRIES)
    .switchMap(() => this.countriesService.getAllCountries())
    .map((countries) => new CountryActions.GetAllCountriesSuccess(countries))
    .catch((error: any) => {
        const errorVo = new ErrorVo({
          code: ErrorCode.GET_COUNTRIES_ERROR,
          type: 'popup',
          errorOrigin: error
        });
        this.actionDispatcher.dispatch(new ErrorActions.Throw(errorVo));
        return Observable.throw(error);
      }
    );

  @Effect()
  getCountryDetailsAndConversionRate$ = this.actions$.ofType(CountryActions.GET_COUNTRY_DETAILS_AND_CONVERSION_RATE)
    .switchMap((action: GetCountryDetailsAndConversionRate) => this.countriesService.getCountryDetails(action.countryCode))
    .catch((error: any) => {
        const errorVo = new ErrorVo({
          code: ErrorCode.GET_COUNTRY_DETAILS_ERROR,
          type: 'popup',
          errorOrigin: error
        });
        this.actionDispatcher.dispatch(new ErrorActions.Throw(errorVo));
        return Observable.throw(errorVo);
      }
    )
    .switchMap((countryDetails: CountryDetailsVo) => {
      this.actionDispatcher.dispatch(new CountryActions.GetCountryDetailsSuccess(countryDetails));
      return this.countriesService.getConversionRate(countryDetails.currencies[0]);
    })
    .map((conversionRate: number) => new CountryActions.GetConversionRateSuccess(conversionRate))
    .catch((error: any) => {
        if (!(error instanceof ErrorVo)) {
          const errorVo = new ErrorVo({
            code: ErrorCode.GET_CONVERSION_RATE_ERROR,
            type: 'popup',
            errorOrigin: error
          });
          this.actionDispatcher.dispatch(new ErrorActions.Throw(errorVo));
        }
        return Observable.throw(error);
      }
    );

  @Effect()
  getCountryDetails$ = this.actions$.ofType(CountryActions.GET_COUNTRY_DETAILS)
    .switchMap((action: GetCountryDetails) => this.countriesService.getCountryDetails(action.countryCode))
    .do((data) => console.log(data))
    .map((countryDetails) => new CountryActions.GetCountryDetailsSuccess(countryDetails))
    .catch((error: any) => {
        const errorVo = new ErrorVo({
          code: ErrorCode.GET_COUNTRY_DETAILS_ERROR,
          type: 'popup',
          errorOrigin: error
        });
        this.actionDispatcher.dispatch(new ErrorActions.Throw(errorVo));
        return Observable.throw(error);
      }
    );

  @Effect()
  getConversionRate$ = this.actions$.ofType(CountryActions.GET_CONVERSION_RATE)
    .switchMap((action: GetConversionRate) => this.countriesService.getConversionRate(action.currency))
    .map((conversionRate) => new CountryActions.GetConversionRateSuccess(conversionRate))
    .catch((error: any) => {
        const errorVo = new ErrorVo({
          code: ErrorCode.GET_CONVERSION_RATE_ERROR,
          type: 'popup',
          errorOrigin: error
        });
        this.actionDispatcher.dispatch(new ErrorActions.Throw(errorVo));
        return Observable.throw(error);
      }
    );
}
