import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {MatDialog} from '@angular/material';
import * as ErrorActions from '../action/error-actions';
import {ErrorDialogComponent} from '../../components/error/error-dialog/error-dialog.component';
import * as RouterActions from '../action/router-actions';
import {RouteNames} from '../../config/route-names';
import {ActionDispatcher} from '../dispatcher/action-dispatcher';

@Injectable()
/**
 * Handles all error actions
 */
export class ErrorEffects {

  constructor(private actions$: Actions,
              public dialog: MatDialog,
              private actionDispatcher: ActionDispatcher) {
  }

  @Effect({dispatch: false})
  throwError$: Observable<any> = this.actions$.ofType(ErrorActions.THROW)
    .map((action: ErrorActions.Throw) => {
      if (action.error.type === 'popup') {
        this.dialog.open(ErrorDialogComponent, {
          width: '30%',
          minWidth: '300px',
          data: action.error,
          disableClose: !action.error.skippable
        });
      } else {
        this.actionDispatcher.dispatch(new RouterActions.Go({
          path: [RouteNames.ERROR, action.error.code]
        }));
      }
    });
}
