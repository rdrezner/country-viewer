import {CountriesState} from './state/countries-state';
import {RouterReducerState} from '@ngrx/router-store';
import {RouterStateUrl} from './serializer/custom-router-state-serializer';

export interface ApplicationStore {
  countriesState: CountriesState;
  routerState: RouterReducerState<RouterStateUrl>;
}
