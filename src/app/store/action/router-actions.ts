import {Action} from '@ngrx/store';
import {NavigationExtras} from '@angular/router';

const TYPE = '[Router] ';

export const GO = TYPE + 'GO';
export const BACK = TYPE + 'BACK';
export const FORWARD = TYPE + 'FORWARD';

export class Go implements Action {
  readonly type = GO;

  constructor(public payload: RouterPayload) {
  }
}

export class Back implements Action {
  readonly type = BACK;
}

export class Forward implements Action {
  readonly type = FORWARD;
}

export type Actions
  = Go
  | Back
  | Forward;

export interface RouterPayload {
  path: any[];
  query?: object;
  extras?: NavigationExtras;
}
