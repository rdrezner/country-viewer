import {Action} from '@ngrx/store';
import {ErrorVo} from '../../model/error-vo';

const TYPE = '[Error] ';

export const THROW = TYPE + 'THROW';

export class Throw implements Action {
  readonly type = THROW;

  constructor(public error: ErrorVo) {
  }
}

export type Actions
  = Error;
