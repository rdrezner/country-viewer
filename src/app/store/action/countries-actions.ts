import {Action} from '@ngrx/store';
import {CountryBriefDataVo} from '../../model/country-brief-data-vo';
import {CountryDetailsVo} from '../../model/country-details-vo';

const TYPE = '[Country] ';

export const GET_ALL_COUNTRIES = TYPE + 'GET_ALL_COUNTRIES';
export const GET_ALL_COUNTRIES_SUCCESS = TYPE + 'GET_ALL_COUNTRIES_SUCCESS';
export const GET_ALL_COUNTRIES_FAIL = TYPE + 'GET_ALL_COUNTRIES_FAIL';

export const GET_COUNTRY_DETAILS_AND_CONVERSION_RATE = TYPE + 'GET_COUNTRY_DETAILS_AND_CONVERSION_RATE';

export const GET_COUNTRY_DETAILS = TYPE + 'GET_COUNTRY_DETAILS';
export const GET_COUNTRY_DETAILS_SUCCESS = TYPE + 'GET_COUNTRY_DETAILS_SUCCESS';
export const GET_COUNTRY_DETAILS_FAIL = TYPE + 'GET_COUNTRY_DETAILS_FAIL';

export const GET_CONVERSION_RATE = TYPE + 'GET_CONVERSION_RATE';
export const GET_CONVERSION_RATE_SUCCESS = TYPE + 'GET_CONVERSION_RATE_SUCCESS';
export const GET_CONVERSION_RATE_FAIL = TYPE + 'GET_CONVERSION_RATE_FAIL';

export class GetAllCountries implements Action {
  readonly type = GET_ALL_COUNTRIES;

}

export class GetAllCountriesSuccess implements Action {
  constructor(public countries: CountryBriefDataVo[]) {
  }

  readonly type = GET_ALL_COUNTRIES_SUCCESS;
}

export class GetAllCountriesFail implements Action {
  readonly type = GET_ALL_COUNTRIES_FAIL;

}

export class GetCountryDetailsAndConversionRate implements Action {
  readonly type = GET_COUNTRY_DETAILS_AND_CONVERSION_RATE;

  constructor(public countryCode: string) {
  }

}

export class GetCountryDetails implements Action {
  readonly type = GET_COUNTRY_DETAILS;

  constructor(public countryCode: string) {
  }

}

export class GetCountryDetailsSuccess implements Action {
  constructor(public countryDetails: CountryDetailsVo) {
  }

  readonly type = GET_COUNTRY_DETAILS_SUCCESS;
}

export class GetCountryDetailsFail implements Action {
  readonly type = GET_COUNTRY_DETAILS_FAIL;

}

export class GetConversionRate implements Action {
  constructor(public currency: string) {
  }

  readonly type = GET_CONVERSION_RATE;

}

export class GetConversionRateSuccess implements Action {
  constructor(public conversionRate: number) {
  }

  readonly type = GET_CONVERSION_RATE_SUCCESS;
}

export class GetConversionRateFail implements Action {
  readonly type = GET_CONVERSION_RATE_FAIL;

}

export type Actions
  = GetAllCountries | GetAllCountriesSuccess | GetAllCountriesFail
  | GetCountryDetails | GetCountryDetailsSuccess | GetCountryDetailsFail
  | GetConversionRate | GetConversionRateSuccess | GetConversionRateFail;
