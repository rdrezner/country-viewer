import {Action} from '@ngrx/store';

const TYPE = '[Component] ';

export const COUNTRY_DETAILS_BACK_SELECTED = TYPE + 'COUNTRY_DETAILS_BACK_SELECTED';

export const COUNTRIES_REFRESH_SELECTED = TYPE + 'COUNTRIES_REFRESH_SELECTED';
export const COUNTRIES_COUNTRY_SELECTED = TYPE + 'COUNTRIES_COUNTRY_SELECTED';


export class CountryDetailsBackSelected implements Action {
  readonly type = COUNTRY_DETAILS_BACK_SELECTED;
}

export class CountriesRefreshSelected implements Action {
  readonly type = COUNTRIES_REFRESH_SELECTED;
}

export class CountriesCountrySelected implements Action {
  readonly type = COUNTRIES_COUNTRY_SELECTED;

  constructor(public countryCode: string) {
  }
}

export type Actions
  = CountryDetailsBackSelected | CountriesRefreshSelected | CountriesCountrySelected;
