import {ActionDispatcher} from './action-dispatcher';
import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';

@Injectable()
/**
 * Abstraction for dispatching actions
 */
export class NgrxActionDispatcher extends ActionDispatcher {

  constructor(private store: Store<any>) {
    super();
  }

  dispatch(action: { type: string }): void {
    console.log('[NgrxActionDispatcher] Dispatching action:', action.type);
    this.store.dispatch(action);
  }
}
