export abstract class ActionDispatcher {

  dispatch(action: { type: string }): void {
    throw new Error('implement abstract class');
  }
}
