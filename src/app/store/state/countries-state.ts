import {CountryBriefDataVo} from '../../model/country-brief-data-vo';
import {CountryDetailsVo} from '../../model/country-details-vo';

/**
 * State connected with countries' data
 */
export interface CountriesState {
  countriesList: CountryBriefDataVo[];
  currentCountryDetails?: CountryDetailsVo;
  conversionRate: number;
}

export const INITIAL_COUNTRIES_STATE: CountriesState = {
  countriesList: [],
  currentCountryDetails: undefined,
  conversionRate: 0
};
