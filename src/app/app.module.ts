import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatPaginatorModule, MatSortModule,
  MatTableModule
} from '@angular/material';
import {StoreModule} from '@ngrx/store';
import {countriesReducer} from './store/reducer/countries-reducer';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {CountriesComponent} from './components/countries/countries.component';
import {CountryDetailsComponent} from './components/country-details/country-details.component';
import {CountriesListComponent} from './components/countries/countries-list/countries-list.component';
import {CamelToRegularPipe} from './pipes/camel-to-regular/camel-to-regular.pipe';
import {FormBeautifierPipe} from './pipes/form-beautifier/form-beautifier.pipe';
import {routerReducer, RouterStateSerializer, StoreRouterConnectingModule} from '@ngrx/router-store';
import {EffectsModule} from '@ngrx/effects';
import {RouterEffects} from './store/effect/router-effects';
import {CustomRouterStateSerializer} from './store/serializer/custom-router-state-serializer';
import {CountriesGuard} from './app-routing/guards/countries-guard';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {CountriesService} from './service/countries/coutries-service';
import {RestCountriesService} from './service/countries/rest-countries-service';
import {CountriesEffects} from './store/effect/countries-effects';
import {ANIMATION_TYPES, LoadingModule} from 'ngx-loading';
import {CountryDetailsGuard} from './app-routing/guards/country-details-guard';
import {APP_CONFIG, AppConfig, AppConfigModule} from './config/app-config.module';
import {RestInterceptor} from './service/rest-interceptor';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {ComponentEffects} from './store/effect/component-effects';
import {NgrxActionDispatcher} from './store/dispatcher/ngrx-action-dispatcher';
import {ActionDispatcher} from './store/dispatcher/action-dispatcher';
import {StoreSelector} from './store/selector/store-selector';
import {NgrxStoreSelector} from './store/selector/ngrx-store-selector';
import {ErrorPageComponent} from './components/error/error-page/error-page.component';
import {ErrorDialogComponent} from './components/error/error-dialog/error-dialog.component';
import {ErrorEffects} from './store/effect/error.effects';


@NgModule({
  declarations: [
    AppComponent,
    CountriesComponent,
    CountryDetailsComponent,
    CountriesListComponent,
    CamelToRegularPipe,
    FormBeautifierPipe,
    ErrorPageComponent,
    ErrorDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatPaginatorModule,
    MatTableModule,
    MatInputModule,
    MatSortModule,
    MatCardModule,
    MatDialogModule,
    EffectsModule.forRoot([
      RouterEffects,
      CountriesEffects,
      ComponentEffects,
      ErrorEffects
    ]),
    StoreModule.forRoot({
      routerState: routerReducer,
      countriesState: countriesReducer
    }),
    AppRoutingModule,
    StoreRouterConnectingModule,
    HttpClientModule,
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.threeBounce
    }),
    AppConfigModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient, APP_CONFIG]
      }
    })
  ],
  providers: [
    {provide: RouterStateSerializer, useClass: CustomRouterStateSerializer},
    CountriesGuard,
    CountryDetailsGuard,
    {provide: CountriesService, useClass: RestCountriesService},
    {provide: HTTP_INTERCEPTORS, useClass: RestInterceptor, multi: true},
    {provide: ActionDispatcher, useClass: NgrxActionDispatcher},
    {provide: StoreSelector, useClass: NgrxStoreSelector}
  ],
  entryComponents: [
    ErrorDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient, appConfig: AppConfig) {
  return new TranslateHttpLoader(http, appConfig.i18nPath);
}
