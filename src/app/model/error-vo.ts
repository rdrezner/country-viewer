import {ErrorCode} from './error-code';

/**
 * Contains all error information
 */
export class ErrorVo {
  code: number;
  skippable = false;
  type: 'newPage' | 'popup' = 'newPage';
  errorOrigin?: any;

  constructor(init?: Partial<ErrorVo>) {
    Object.assign(<Object>this, init);
  }

  public get userMessage(): string {
    return (this.code) ? `ERROR.ERROR_${this.code}` : `ERROR.ERROR_${ErrorCode.UNKNOWN_ERROR}`;
  }
}
