/**
 * Country brief data value object
 * {@link https://market.mashape.com/fayder/rest-countries-v1}
 */
export interface CountryBriefDataVo {
  alpha3Code: string;
  name: string;
  capital: string;
  region: string;
}
