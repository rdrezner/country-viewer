/**
 * Error codes constants
 */
export class ErrorCode {

  public static readonly UNKNOWN_ERROR = 1000;

  public static readonly GET_COUNTRIES_ERROR = 1001;
  public static readonly GET_COUNTRY_DETAILS_ERROR = 1002;
  public static readonly GET_CONVERSION_RATE_ERROR = 1003;

  public static readonly GLOBAL_ERROR = 2000;
}
