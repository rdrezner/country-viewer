/**
 * Translation value object
 * {@link https://market.mashape.com/fayder/rest-countries-v1}
 */
export interface TranslationsVo {
  de: string;
  es: string;
  fr: string;
  ja: string;
  it: string;
}

/**
 * Country details value object
 * {@link https://market.mashape.com/fayder/rest-countries-v1}
 */
export interface CountryDetailsVo {
  name: string;
  topLevelDomain: string[];
  alpha2Code: string;
  alpha3Code: string;
  callingCodes: string[];
  capital: string;
  altSpellings: string[];
  region: string;
  subregion: string;
  population: number;
  latlng: number[];
  demonym: string;
  area: number;
  gini: number;
  timezones: string[];
  borders: string[];
  nativeName: string;
  numericCode: string;
  currencies: string[];
  languages: string[];
  translations: TranslationsVo;
  relevance: string;
}


