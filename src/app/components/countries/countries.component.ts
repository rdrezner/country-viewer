import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CountryBriefDataVo} from '../../model/country-brief-data-vo';
import {CountriesState} from '../../store/state/countries-state';
import * as ComponentActions from '../../store/action/component-actions';
import {ActionDispatcher} from '../../store/dispatcher/action-dispatcher';
import {StoreSelector} from '../../store/selector/store-selector';
import {APP_CONFIG, AppConfig} from '../../config/app-config.module';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountriesComponent implements OnInit {

  countriesList$: Observable<CountryBriefDataVo[]>;

  constructor(private actionDispatcher: ActionDispatcher,
              private storeSelector: StoreSelector,
              @Inject(APP_CONFIG) private appConfig: AppConfig) {
    this.countriesList$ = this.storeSelector.countriesState
      .map((countriesState: CountriesState) => countriesState.countriesList);
  }

  ngOnInit() {
  }

  resolveCountrySelected(countryCode: string) {
    this.actionDispatcher.dispatch(new ComponentActions.CountriesCountrySelected(countryCode));
  }

  resolveRefreshSelected() {
    this.actionDispatcher.dispatch(new ComponentActions.CountriesRefreshSelected());
  }

}
