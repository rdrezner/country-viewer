import {AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {CountryBriefDataVo} from '../../../model/country-brief-data-vo';

@Component({
  selector: 'app-countries-list',
  templateUrl: './countries-list.component.html',
  styleUrls: ['./countries-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountriesListComponent implements AfterViewInit {

  displayedColumns = ['alpha3Code', 'name', 'capital', 'region', 'detail-button'];
  dataSource = new MatTableDataSource<CountryBriefDataVo>();

  @Input('countries')
  set countries(value: CountryBriefDataVo[]) {
    this.dataSource.data = value;
  }

  @Input('paginatorConfig')
  set paginatorConfig(value: { pageSize: number, pageSizeOptions: number[] }) {
    this.paginator.pageSize = value.pageSize;
    this.paginator.pageSizeOptions = value.pageSizeOptions;
  }

  @Output('countrySelected')
  countrySelected: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  detailsSelected(countryCode: string, $event: MouseEvent) {
    $event.stopPropagation();
    this.countrySelected.emit(countryCode);
  }
}

