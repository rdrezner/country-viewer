import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CountriesListComponent} from './countries-list.component';
import {MatInputModule, MatPaginatorModule, MatSortModule, MatTableModule} from '@angular/material';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ANIMATION_TYPES, LoadingModule} from 'ngx-loading';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../../app.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {APP_CONFIG, AppConfigModule} from '../../../config/app-config.module';
import {countriesListMock} from '../../../../test/countries-api-responses-mock';
import {By} from '@angular/platform-browser';

describe('CountriesListComponent', () => {
  let component: CountriesListComponent;
  let fixture: ComponentFixture<CountriesListComponent>;
  const countries = countriesListMock;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CountriesListComponent],
      imports: [
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatTableModule,
        MatInputModule,
        MatSortModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppConfigModule,
        LoadingModule.forRoot({
          animationType: ANIMATION_TYPES.threeBounce
        }),
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient, APP_CONFIG]
          }
        })
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountriesListComponent);
    component = fixture.componentInstance;
    component.countries = countries;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render given countries list', () => {
    const targetElements = fixture.debugElement.queryAll(By.css('mat-row'));
    expect(countries.length).toEqual(targetElements.length);
    targetElements.forEach((row, rowIndex) => {
      row.queryAll(By.css('mat-cell')).forEach((cell, cellIndex) => {
        const cellElement = <HTMLElement>(cell.nativeElement);
        if (cellIndex < component.displayedColumns.length - 1) {
          expect(cellElement.innerText).toEqual(countries[rowIndex][component.displayedColumns[cellIndex]]);
        }
      });
    });
  });

  it('should create Details buttons', () => {
    const targetElements = fixture.debugElement.queryAll(By.css('.detail-button'));
    expect(countries.length).toEqual(targetElements.length);
  });

  it('should emit event when row clicked', () => {
    spyOn(component.countrySelected, 'emit').and.callThrough();

    const rows = fixture.debugElement.queryAll(By.css('mat-row'));
    if (rows.length > 0) {
      rows[0].triggerEventHandler('click', new MouseEvent('click', {}));
    }
    expect(component.countrySelected.emit).toHaveBeenCalledWith(countries[0].alpha3Code);

  });

  it('should emit event when details button clicked', () => {
    spyOn(component.countrySelected, 'emit').and.callThrough();

    const detailsButtons = fixture.debugElement.queryAll(By.css('.detail-button'));
    if (detailsButtons.length > 0) {
      detailsButtons[0].triggerEventHandler('click', new MouseEvent('click', {}));
    }
    expect(component.countrySelected.emit).toHaveBeenCalledWith(countries[0].alpha3Code);
  });
});
