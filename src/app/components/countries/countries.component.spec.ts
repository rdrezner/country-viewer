import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CountriesComponent} from './countries.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ANIMATION_TYPES, LoadingModule} from 'ngx-loading';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../app.module';
import {CountriesListComponent} from './countries-list/countries-list.component';
import {MatButtonModule, MatInputModule, MatPaginatorModule, MatSortModule, MatTableModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {StoreSelector} from '../../store/selector/store-selector';
import {StoreSelectorMock} from '../../../test/store-selector.mock';
import {ActionDispatcherMock} from '../../../test/action-dispatcher-mock';
import {ActionDispatcher} from '../../store/dispatcher/action-dispatcher';
import {APP_CONFIG, AppConfigModule} from '../../config/app-config.module';
import {By} from '@angular/platform-browser';
import * as ComponentActions from '../../store/action/component-actions';

describe('CountriesComponent', () => {
  let component: CountriesComponent;
  let fixture: ComponentFixture<CountriesComponent>;
  const actionDispatcher = new ActionDispatcherMock();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CountriesComponent,
        CountriesListComponent
      ],
      imports: [
        MatInputModule,
        MatTableModule,
        MatButtonModule,
        MatPaginatorModule,
        MatTableModule,
        MatInputModule,
        MatSortModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppConfigModule,
        LoadingModule.forRoot({
          animationType: ANIMATION_TYPES.threeBounce
        }),
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient, APP_CONFIG]
          }
        })
      ],
      providers: [
        {provide: ActionDispatcher, useValue: actionDispatcher},
        {provide: StoreSelector, useClass: StoreSelectorMock}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create Refresh button', () => {
    const refreshButton = fixture.debugElement.query(By.css('#refresh-button'));
    expect(refreshButton).toBeTruthy();
  });

  it('should create Countries list component', () => {
    const countriesList = fixture.debugElement.query(By.css('app-countries-list'));
    expect(countriesList).toBeTruthy();
  });

  it('should dispatch action when Refresh button clicked', () => {
    const refreshButton = fixture.debugElement.query(By.css('#refresh-button'));

    spyOn(component, 'resolveRefreshSelected').and.callThrough();
    spyOn(actionDispatcher, 'dispatch');

    refreshButton.triggerEventHandler('click', null);

    expect(component.resolveRefreshSelected).toHaveBeenCalled();
    expect(actionDispatcher.dispatch).toHaveBeenCalledWith(new ComponentActions.CountriesRefreshSelected());
  });

  it('should dispatch action when country selected', () => {
    const countriesList = fixture.debugElement.query(By.css('app-countries-list'));
    const countryCode = 'countryCode';

    spyOn(component, 'resolveCountrySelected').and.callThrough();
    spyOn(actionDispatcher, 'dispatch');

    countriesList.componentInstance.countrySelected.emit(countryCode);

    expect(component.resolveCountrySelected).toHaveBeenCalledWith(countryCode);
    expect(actionDispatcher.dispatch).toHaveBeenCalledWith(new ComponentActions.CountriesCountrySelected(countryCode));
  });
});
