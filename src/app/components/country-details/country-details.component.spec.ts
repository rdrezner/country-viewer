import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CountryDetailsComponent} from './country-details.component';
import {ANIMATION_TYPES, LoadingModule} from 'ngx-loading';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../app.module';
import {MatCardModule, MatInputModule} from '@angular/material';
import {CamelToRegularPipe} from '../../pipes/camel-to-regular/camel-to-regular.pipe';
import {FormBeautifierPipe} from '../../pipes/form-beautifier/form-beautifier.pipe';
import {ActionDispatcherMock} from '../../../test/action-dispatcher-mock';
import {ActionDispatcher} from '../../store/dispatcher/action-dispatcher';
import {StoreSelector} from '../../store/selector/store-selector';
import {StoreSelectorMock} from '../../../test/store-selector.mock';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {APP_CONFIG, APP_DI_CONFIG, AppConfigModule} from '../../config/app-config.module';
import {By} from '@angular/platform-browser';
import * as ComponentActions from '../../store/action/component-actions';
import {DecimalPipe} from '@angular/common';

describe('CountryDetailsComponent', () => {
  let component: CountryDetailsComponent;
  let fixture: ComponentFixture<CountryDetailsComponent>;
  const actionDispatcher = new ActionDispatcherMock();
  const storeSelector = new StoreSelectorMock();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CountryDetailsComponent,
        CamelToRegularPipe,
        FormBeautifierPipe
      ],
      imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        MatCardModule,
        MatInputModule,
        LoadingModule.forRoot({
          animationType: ANIMATION_TYPES.threeBounce
        }),
        AppConfigModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient, APP_CONFIG]
          }
        })
      ],
      providers: [
        {provide: ActionDispatcher, useValue: actionDispatcher},
        {provide: StoreSelector, useValue: storeSelector}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create Back button', () => {
    const backButton = fixture.debugElement.query(By.css('.back-button'));
    expect(backButton).toBeTruthy();
  });

  it('should create Conversion card', () => {
    const conversionCard = fixture.debugElement.query(By.css('.conversion-card'));
    expect(conversionCard).toBeTruthy();
  });

  it('should dispatch action when Back button clicked', () => {
    const backButton = fixture.debugElement.query(By.css('.back-button'));

    spyOn(component, 'resolveBackClick').and.callThrough();
    spyOn(actionDispatcher, 'dispatch');

    backButton.triggerEventHandler('click', null);

    expect(component.resolveBackClick).toHaveBeenCalled();
    expect(actionDispatcher.dispatch).toHaveBeenCalledWith(new ComponentActions.CountryDetailsBackSelected());
  });

  it('should render given country details', () => {
    const targetElements = fixture.debugElement.queryAll(By.css('mat-form-field'));
    const formBeautifierPipe = new FormBeautifierPipe();
    component.details$.subscribe(details => {
        targetElements.forEach((formElement, formIndex) => {
          const input = formElement.query(By.css('input'));
          if (details) {
            expect((<HTMLInputElement>(input.nativeElement)).value)
              .toEqual('' + (formBeautifierPipe.transform(details[formIndex].value, details[formIndex].key)));
          }
        });
      }
    );
  });

  it('should render given conversion rate', () => {
    const conversionRateElement = fixture.debugElement.query(By.css('#conversion-rate'));
    const numberPipe = new DecimalPipe(APP_DI_CONFIG.defaultLocale);
    component.conversionRate$.subscribe(conversionRate => {
        expect((<HTMLElement>(conversionRateElement.nativeElement)).innerText)
          .toEqual('' + numberPipe.transform(conversionRate, '1.0-2'));
      }
    );
  });
});
