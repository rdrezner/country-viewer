import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {CountriesState} from '../../store/state/countries-state';
import {CountryDetailsVo} from '../../model/country-details-vo';
import * as ComponentActions from '../../store/action/component-actions';
import {StoreSelector} from '../../store/selector/store-selector';
import {ActionDispatcher} from '../../store/dispatcher/action-dispatcher';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountryDetailsComponent implements OnInit {

  details$: Observable<{ key: string, value: string }[] | undefined>;
  conversionRate$: Observable<number>;

  constructor(private actionDispatcher: ActionDispatcher,
              private storeSelector: StoreSelector) {

    /**
     * Transforming state to observable that is more comfortable for template
     */
    this.details$ = this.storeSelector.countriesState
      .map((countriesState: CountriesState) => countriesState.currentCountryDetails)
      .map((countryDetails: CountryDetailsVo) => {
        if (!countryDetails) {
          return undefined;
        }
        return Object.keys(countryDetails)
          .map((key: string) => {
            return {key: key, value: countryDetails[key]};
          });
      });

    this.conversionRate$ = this.storeSelector.countriesState
      .map((countriesState: CountriesState) => countriesState.conversionRate);
  }

  ngOnInit() {
  }

  resolveBackClick() {
    this.actionDispatcher.dispatch(new ComponentActions.CountryDetailsBackSelected());
  }

}
