import {Component, OnInit} from '@angular/core';
import {StoreSelector} from '../../../store/selector/store-selector';
import {Observable} from 'rxjs/Observable';
import {RouterStateUrl} from '../../../store/serializer/custom-router-state-serializer';
import {RouterReducerState} from '@ngrx/router-store';
import {RouteNames} from '../../../config/route-names';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit {

  errorCode$: Observable<string>;

  constructor(private storeSelector: StoreSelector) {
  }

  ngOnInit() {
    this.errorCode$ = this.storeSelector.routerState
      .map((routerState: RouterReducerState<RouterStateUrl>) => routerState.state.params[RouteNames.ERROR_PARAM_CODE]);
  }

}
