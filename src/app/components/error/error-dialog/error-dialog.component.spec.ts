import {async, TestBed} from '@angular/core/testing';

import {ErrorDialogComponent} from './error-dialog.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../../app.module';
import {NgModule} from '@angular/core';
import {MatDialog, MatDialogModule} from '@angular/material';
import {ErrorCode} from '../../../model/error-code';
import {ErrorVo} from '../../../model/error-vo';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {APP_CONFIG, AppConfigModule} from '../../../config/app-config.module';

@NgModule({
  entryComponents: [
    ErrorDialogComponent
  ],
})
class DialogTestModule { }

describe('ErrorDialogComponent', () => {
  let component: ErrorDialogComponent;
  let dialog: MatDialog;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorDialogComponent ],
      imports: [
        DialogTestModule,
        MatDialogModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppConfigModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient, APP_CONFIG]
          }
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    dialog = TestBed.get(MatDialog);
    const errorData = new ErrorVo({code: ErrorCode.UNKNOWN_ERROR});
    const dialogRef = dialog.open(ErrorDialogComponent, {
      data: errorData,
      height: '400px',
      width: '600px',
    });
    component = dialogRef.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
