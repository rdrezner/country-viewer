import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CountriesComponent} from '../components/countries/countries.component';
import {CountryDetailsComponent} from '../components/country-details/country-details.component';
import {CountriesGuard} from './guards/countries-guard';
import {CountryDetailsGuard} from './guards/country-details-guard';
import {RouteNames} from '../config/route-names';
import {ErrorPageComponent} from '../components/error/error-page/error-page.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: RouteNames.COUNTRIES,
    pathMatch: 'full'
  },
  {
    path: RouteNames.COUNTRIES,
    component: CountriesComponent,
    canActivate: [CountriesGuard]
  },
  {
    path: RouteNames.COUNTRY_DETAILS + '/:' + RouteNames.COUNTRY_DETAILS_PARAM_CODE,
    component: CountryDetailsComponent,
    canActivate: [CountryDetailsGuard]
  },
  {
    path: RouteNames.ERROR + '/:' + RouteNames.ERROR_PARAM_CODE,
    component: ErrorPageComponent
  },
  {
    path: '**',
    redirectTo: RouteNames.COUNTRIES,
    pathMatch: 'full'
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})

/**
 * Contains application routing configuration
 */
export class AppRoutingModule {
}
