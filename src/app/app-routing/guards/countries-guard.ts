import {CanActivate} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/first';
import {CountriesState} from '../../store/state/countries-state';
import * as CountryActions from '../../store/action/countries-actions';
import {ActionDispatcher} from '../../store/dispatcher/action-dispatcher';
import {StoreSelector} from '../../store/selector/store-selector';
import * as ErrorActions from '../../store/action/error-actions';
import {ErrorCode} from '../../model/error-code';
import {ErrorVo} from '../../model/error-vo';

/**
 * Triggers action that loads all countries if they aren't in cache
 */
@Injectable()
export class CountriesGuard implements CanActivate {
  constructor(private actionDispatcher: ActionDispatcher,
              private storeSelector: StoreSelector) {}

  getFromStoreOrAPI(): Observable<any> {
    return this.storeSelector.countriesState
      .first()
      .do((countriesState: CountriesState) => {
        if (!countriesState.countriesList.length) {
          this.actionDispatcher.dispatch(new CountryActions.GetAllCountries());
        }
      })
      .take(1);
  }

  canActivate(): Observable<boolean> {
    return this.getFromStoreOrAPI()
      .switchMap(() => Observable.of(true))
      .catch((data) => {
        console.log(data);
        this.actionDispatcher.dispatch(new ErrorActions.Throw(new ErrorVo({
          type: 'newPage',
          code: ErrorCode.GLOBAL_ERROR
        })));
        return Observable.of(false);
      });
  }
}
