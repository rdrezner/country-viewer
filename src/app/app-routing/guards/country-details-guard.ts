import {CanActivate} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/catch';
import {CountriesState} from '../../store/state/countries-state';
import * as CountryActions from '../../store/action/countries-actions';
import {RouterStateUrl} from '../../store/serializer/custom-router-state-serializer';
import {RouterReducerState} from '@ngrx/router-store';
import {StoreSelector} from '../../store/selector/store-selector';
import {ActionDispatcher} from '../../store/dispatcher/action-dispatcher';
import * as ErrorActions from '../../store/action/error-actions';
import {ErrorCode} from '../../model/error-code';
import {ErrorVo} from '../../model/error-vo';
import {RouteNames} from '../../config/route-names';

/**
 * Triggers action that loads country details if they aren't in cache
 */
@Injectable()
export class CountryDetailsGuard implements CanActivate {
  constructor(private actionDispatcher: ActionDispatcher,
              private storeSelector: StoreSelector) {
  }

  getFromStoreOrAPI(countryCode: string): Observable<any> {
    return this.storeSelector.countriesState
      .first()
      .do((state: CountriesState) => {
        if (!state.currentCountryDetails || state.currentCountryDetails.alpha3Code !== countryCode.toUpperCase()) {
          this.actionDispatcher.dispatch(new CountryActions.GetCountryDetailsAndConversionRate(countryCode));
        }
      })
      .take(1);
  }

  canActivate(): Observable<boolean> {
    return this.storeSelector.routerState
      .map((routerState: RouterReducerState<RouterStateUrl>) => routerState.state)
      .switchMap((routerStateUrl: RouterStateUrl) =>
        this.getFromStoreOrAPI(routerStateUrl.params[RouteNames.COUNTRY_DETAILS_PARAM_CODE]))
      .switchMap(() => Observable.of(true))
      .catch((data) => {
        console.log(data);
        this.actionDispatcher.dispatch(new ErrorActions.Throw(new ErrorVo({
          type: 'newPage',
          code: ErrorCode.GLOBAL_ERROR
        })));
        return Observable.of(false);
      });
  }
}
